# Thought process

## What I need
I need to block access to a certain website depending on the value of a cell from a Google Sheets spreadsheet that corresponds to today's date.

## How to achieve that
When I think of "block access to a certain website", three ideas immediately come to mind.
1. Use one of the popular browser extensions for blocking distractions.
2. Modify the hosts file on my system.
3. Block the website through the firewall/content filtering settings of my router.

While writing this document, 2 more ideas popped up on my mind:

4. Use the Operating System's built-in firewall
5. Use a tool like Screen Time (https://support.apple.com/en-us/HT210387)

Option #2 sounds like one of the easiest one to implement programatically. For example, it would be trivial to use a browser extension to block a website, but it sounds like it would take a lot of work to figure out how to dynamically change the blocklist of the extension without any user input, while the hosts file on my computer is a simple text file that can be trivially modified by any script (as long as the user running the script has "write" permission to the hosts file).


## Programatically modifying the hosts file
After a Google search, I found out that on my Linux machine the hosts file is located at "/etc/hosts".
I already knew that entries in the hosts file are formatted as "IP_ADDRESS HOSTNAME" (e.g. "127.0.0.1 localhost"), but if I didn't know that yet, I could have easily found out about it from [Wikipedia](https://en.wikipedia.org/wiki/Hosts_(file)#File_content).
I opened the file with a text editor and added 2 new lines:
127.0.0.1  https://twitter.com
127.0.0.1  twitter.com

Note that "127.0.0.1" is an IPv4 address that always points to the local network interface ([More Info](https://en.wikipedia.org/wiki/Localhost)).

## Testing my modification to the hosts file
I saved the modified version of the hosts file, opened a new tab in Firefox, typed "twitter.com", pressed Enter, and... huh? It was still loading. Could it be that my DNS resolver had cached the IP address for Twitter? I opened a terminal window, ran "ping twitter.com", and verified that "ping" was connecting to 127.0.0.1 rather than Twitter's own server. Ah, so it must be the browser's DNS cache that needs to be cleared. After a brief search, I found out that Firefox only stores its DNS cache in memory, which means that it will be automatically cleared every time Firefox is closed/restarted.
After restarting Firefox, Twitter was no longer loading! Well, technically some components were still being displayed, since Twitter automatically installs offline [Service Workers](https://developers.google.com/web/fundamentals/primers/service-workers) that still load the layout components even without Internet connectivity, but no tweets, messages, or users were being loaded. Instead, all I could see was an error message of "Something went wrong. Try reloading."
Chrome also seems to require a restart to clear its DNS cache.

## DNS over HTTPS
Incidentally, during one of my previous Internet searches, I also found out that Firefox will not respect the system's hosts file if the network option "DNS-over-HTTPS" is enabled on Firefox. As it happens, I already had that option disabled for unrelated reasons.

## Alternative to restarting
If I had a strong aversion to needing to restart my browsers for my twitter block to take effect, I could disable DNS caching on Firefox and Chrome. This would lead to a slight increase in loading times when browsing the web, but it is nonetheless an option.

## Accessing my spreadsheet
Figuring out how to connect to Google's API was a PITA. Google's official documentation wasn't too helpful. Thankfully, I found a short [blog post](https://denisluiz.medium.com/python-with-google-sheets-service-account-step-by-step-8f74c26ed28e) that ended up being very useful.
Per the post, I had to create a Google Service Account with a role of "Editor", enable access to the Google Sheets API, download a JSON file with the service account's secret key, and share my Spreadsheet with the email associated to the Service Account.


## Dependencies
I installed the required dependencies with the Python package manager PIP:

    pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib

I ran this command inside a python virtual environment (venv), though that's not necessary.

## Coding
After reading the post, it wasn't too hard to write a script that modifies the hosts file depending on the data from a Google Sheet.

## Running the script/File permissions
Since the hosts file can only be written to by the root account, the python script must be run with "sudo" (i.e. "sudo python3 spreadsheet_site_blocker.py")

## Automating script execution
The script could be configured to be run automatically (e.g. at the start of every day) using a job scheduler like Cron or Launchd. Some links that could be helpful:

https://superuser.com/questions/126907/how-can-i-get-a-script-to-run-every-day-on-mac-os-x

https://alvinalexander.com/mac-os-x/launchd-examples-launchd-plist-file-examples-mac/

https://alvinalexander.com/mac-os-x/launchd-plist-examples-startinterval-startcalendarinterval/

https://medium.com/@chetcorcos/a-simple-launchd-tutorial-9fecfcf2dbb3

https://hankehly.wordpress.com/2016/08/20/launchd-startcalendarinterval-asleep-vs-power-off/
